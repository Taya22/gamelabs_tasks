using UnityEngine;

namespace Task_3.Scripts.Weapons
{
    [CreateAssetMenu(menuName = "Task_3/Ship Weapon")]
    public class ShipWeapon : ScriptableObject
    {
        [SerializeField] private ActiveGun weaponPrefab;
        public ActiveGun WeaponPrefab => weaponPrefab;

        [SerializeField] public string weaponName;
        [SerializeField] public Sprite sprite;
        [SerializeField] [TextArea] public string description;
    }
}
