using System;
using UnityEngine;

namespace Task_3.Scripts.Weapons
{
    public class ActiveGun : MonoBehaviour
    {
        [SerializeField] private ParticleSystem ps;
        [SerializeField] private float baseWeaponCd = 2f;
        [SerializeField] private int damage = 2;
        public int GetDamage => damage;

        public float weaponCd = 2f;
        
        private float shootCooldown;
        private bool canShoot;
        private bool weaponActive;

        private void Start()
        {
            weaponCd = baseWeaponCd;
        }

        private void Update()
        {
            if (!canShoot)
            {
                shootCooldown -= Time.deltaTime;
                if (shootCooldown <= 0)
                {
                    canShoot = true;
                    shootCooldown = weaponCd;
                }
            }
        }

        public void SetWeaponActive(bool isActive)
        {
            weaponActive = isActive;
        }

        public void ModifyWeaponCooldown(float miltiplier)
        {
            weaponCd *= miltiplier;
        }

        public void ResetWeaponCD()
        {
            weaponCd = baseWeaponCd;
        }

        public void Shoot()
        {
            if(!weaponActive || !canShoot) return;
            ps.Play();
            canShoot = false;
        }
    }
}
