using System;
using System.Collections;
using System.Collections.Generic;
using Task_3.Scripts.Modules;
using Task_3.Scripts.Ship;
using Task_3.Scripts.Weapons;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Task_3.Scripts
{
    public class ModularShip : MonoBehaviour
    {
        public enum StatType { Health, Shield, WeaponCooldown, ShieldRestoration }

        public Action<StatType, float> OnStatValueChange;

        
        [Header("Stats")]
        [SerializeField] private int maxHp = 100;
        [SerializeField] private int maxShield = 80;
        [SerializeField] private float moveSpeed;
        [SerializeField] private float baseSecondsToRestoreShield = 1f;
        [SerializeField] private float weaponCdMultiplier = 1f;
        [SerializeField] private float shieldRestoreMultiplier = 1f;

        [Header("Current Stats")] //serialized for debug purposes
        [SerializeField] private int curHp;
        [SerializeField] private int curShield;
        [SerializeField] private float curSecondsToRestoreShield = 1f;

        [Header("Other Settings")] 
        [SerializeField] private ShipCanvas shipCanvas;
        [SerializeField] private ParticleSystem thrustParticles;

        [SerializeField] private float xMinPos;
        [SerializeField] private float xMaxPos;
        
        [Header("Modules")]
        [SerializeField] private int modulesSlots = 2;
        [SerializeField] private int weaponSlots = 2;
        //[SerializeField] private Transform modulesParent;
        [SerializeField] private Transform[] weaponPoints;

        private ShipWeapon[] _weapons;
        private List<ActiveGun> instantiatedGuns = new List<ActiveGun>();
        private List<ShipModule> _modules = new List<ShipModule>();
        
        public int ModulesSlots => modulesSlots;
        public int WeaponSlots => weaponSlots;
        
        private bool shipSpawned;
        private bool playerShip;
        private float shieldRestoreTimer;

        private void Awake()
        {
            Initialize();
        }
        
        private void Initialize()
        {
            curHp = maxHp;
            curShield = maxShield;

            _weapons = new ShipWeapon[weaponSlots];
        }
        
        protected virtual void Update()
        {
            if(!shipSpawned || curHp <= 0) return;
            
            if (curShield < maxShield)
            { 
                RestoreShield();
            }

            if (playerShip)
            {
                UpdateInput();
            }
            else
            {
                foreach (var activeGun in instantiatedGuns)
                {
                    activeGun.Shoot();
                }
            }
        }
        
        public virtual void OnSpawnedFromHangar(bool isPlayerShip)
        {
            shipCanvas.Initialize(maxHp, maxShield);
            shipCanvas.gameObject.SetActive(true);
            shipSpawned = true;
            playerShip = isPlayerShip;
            
            thrustParticles.Play();
            SetWeaponActive(true);
            ApplyWeaponCdBonus();
            
            if(!playerShip)
                StartCoroutine(StartRandomMovement());
        }

        public virtual void OnReturnToHangar()
        {
            shipCanvas.gameObject.SetActive(false);
            shipSpawned = false;
            playerShip = false;

            curHp = maxHp;
            curShield = maxShield;
            SetWeaponActive(false);
            ResetWeaponCd();
            
            thrustParticles.Stop();
        }

        private void ApplyWeaponCdBonus()
        {
            foreach (var activeGun in instantiatedGuns)
            {
                activeGun.ModifyWeaponCooldown(weaponCdMultiplier);
            }
        }

        private void ResetWeaponCd()
        {
            foreach (var activeGun in instantiatedGuns)
            {
                activeGun.ResetWeaponCD();
            }
        }

        private void RestoreShield()
        {
            shieldRestoreTimer -= Time.deltaTime;
            if (shieldRestoreTimer <= 0)
            {
                curShield++;
                shieldRestoreTimer = curSecondsToRestoreShield;
            }
        }

        private void UpdateInput()
        {
            if (Input.GetButton("Fire1"))
            {
                foreach (var activeGun in instantiatedGuns)
                {
                    activeGun.Shoot();
                }
            }
            
            var xMove = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
            var yMove = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
            var moveDir = new Vector3(xMove, yMove, 0);

            UpdatePosition(moveDir);
        }

        private IEnumerator StartRandomMovement()
        {
            var timer = 3f;
            
            var randomX = 0f;
            var randomY = 0f;
            
            while (shipSpawned)
            {
                timer += Time.deltaTime;
                UpdatePosition(new Vector3(randomX, randomY, 0));
                if (timer >= 1.5f)
                {
                    timer = 0;
                    randomX = (float)Random.Range(-100, 100) / 100 * moveSpeed * Time.deltaTime;
                    randomY = (float)Random.Range(-100, 100) / 100 * moveSpeed * Time.deltaTime;
                }

                yield return null;
            }
        }

        private void UpdatePosition(Vector3 moveDir)
        {
            var clampedX = Mathf.Clamp(transform.position.x + moveDir.x, xMinPos, xMaxPos);
            var clampedY = Mathf.Clamp(transform.position.y + moveDir.y, -17, 17);
            transform.position = new Vector3(clampedX, clampedY, 0);
        }
        
         public void AttachModule(ShipModule module)
        {
            _modules.Add(module);

            var newValue = 0f;
            switch (module.GetStatType)
            {
                case StatType.Health:
                    newValue = maxHp = (int)module.Apply(maxHp);
                    break;
                case StatType.Shield:
                    newValue = maxShield = (int)module.Apply(maxShield);
                    break;
                case StatType.WeaponCooldown:
                    newValue = weaponCdMultiplier = module.Apply(weaponCdMultiplier);
                    break;
                case StatType.ShieldRestoration:
                    newValue = shieldRestoreMultiplier = module.Apply(shieldRestoreMultiplier);
                    curSecondsToRestoreShield = baseSecondsToRestoreShield * shieldRestoreMultiplier;
                    break;
            }

            OnStatValueChange?.Invoke(module.GetStatType, newValue);
        }
       
        public void DetachModule(ShipModule module)
        {
            _modules.Remove(module);

            var newValue = 0f;
            switch (module.GetStatType)
            {
                case StatType.Health:
                    newValue = maxHp = (int)module.Detach(maxHp);
                    break;
                case StatType.Shield:
                    newValue = maxShield = (int)module.Detach(maxShield);
                    break;
                case StatType.WeaponCooldown:
                    newValue = weaponCdMultiplier = module.Detach(weaponCdMultiplier);
                    break;
                case StatType.ShieldRestoration:
                    newValue = shieldRestoreMultiplier = module.Detach(shieldRestoreMultiplier);
                    curSecondsToRestoreShield = baseSecondsToRestoreShield * shieldRestoreMultiplier;
                    break;
            }
            
            OnStatValueChange?.Invoke(module.GetStatType, newValue);
        }


        public void AttachWeapon(ShipWeapon weapon)
        {
            var pointIndex = 0;
            for (var i = 0; i < weaponSlots; i++)
            {
                if (_weapons[i] != null) continue;
                _weapons[i] = weapon;
                pointIndex = i;
            }

            var curPoint = weaponPoints[pointIndex];
            for (var i = 0; i < curPoint.childCount; i++)
            {
                var child = curPoint.GetChild(i);
                if (child.name == weapon.WeaponPrefab.name)
                {
                    child.gameObject.SetActive(true);
                    return;
                }
            }

            var newWeapon = Instantiate(weapon.WeaponPrefab, Vector3.zero, Quaternion.identity);
            newWeapon.name = weapon.WeaponPrefab.name;
            newWeapon.transform.SetParent(curPoint, false);
            instantiatedGuns.Add(newWeapon);
        }
        
        public void DetachWeapon(ShipWeapon weapon)
        {
            var pointIndex = 0;
            for (var i = 0; i < weaponSlots; i++)
            {
                if (_weapons[i] != weapon) continue;
                _weapons[i] = null;
                pointIndex = i;
            }
            
            for (var i = 0; i < weaponPoints[pointIndex].childCount; i++)
            {
                var child = weaponPoints[pointIndex].GetChild(i);
                if (child.name == weapon.WeaponPrefab.name)
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
        
        private void SetWeaponActive(bool isActive)
        {
            foreach (var activeGun in instantiatedGuns)
            {
                activeGun.SetWeaponActive(isActive);
            }
        }
        
        private void OnParticleCollision(GameObject other)
        {
            var activeGun = other.GetComponentInParent<ActiveGun>();
            if (activeGun.transform.root != transform)
            {
                OnHit(activeGun.GetDamage);
            }
        }

        private void OnHit(int damage)
        {
            if (curShield <= 0) {
                curHp -= damage;
                shipCanvas.SetHpSliderValue(curHp);
                CheckIfDead();
                return;
            }

            curShield -= damage;
            if (curShield < 0) {
                curHp -= Mathf.Abs(curShield);
                shipCanvas.SetHpSliderValue(curHp);
                curShield = 0;
                CheckIfDead();
            }
            
            shipCanvas.SetShieldSliderValue(curShield);
        }

        private void CheckIfDead()
        {
            if(curHp > 0) return;
            StopAllCoroutines();
            SetWeaponActive(false);
            EventManager.InvokeOnShipDestroyed(playerShip);
        }
        
        public int GetShipHealth() => maxHp;
        public int GetShipShield() => maxShield;
    }
}
