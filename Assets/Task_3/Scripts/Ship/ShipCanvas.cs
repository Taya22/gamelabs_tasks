using UnityEngine;
using UnityEngine.UI;

namespace Task_3.Scripts.Ship
{
    public class ShipCanvas : MonoBehaviour
    {
        [SerializeField] private Slider hpSlide;
        [SerializeField] private Slider shieldSlide;

        public void Initialize(int maxHp, int maxShield)
        {
            hpSlide.maxValue = maxHp;
            hpSlide.value = maxHp;
            shieldSlide.maxValue = maxShield;
            shieldSlide.value = maxShield;
        }
    
        public void SetHpSliderValue(int curHp)
        {
            hpSlide.value = curHp;
        }
    
        public void SetShieldSliderValue(int cuhShield)
        {
            shieldSlide.value = cuhShield;
        }
    }
}
