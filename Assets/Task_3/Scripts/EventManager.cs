using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class EventManager
{
    public static UnityEvent<bool> OnShipDestroyed = new UnityEvent<bool>();
    public static void InvokeOnShipDestroyed(bool playerShip)
    {
        OnShipDestroyed.Invoke(playerShip);
    }
}
