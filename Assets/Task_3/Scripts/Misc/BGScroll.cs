using System;
using UnityEngine;
using UnityEngine.UI;

namespace Task_3.Scripts.Misc
{
    public class BGScroll : MonoBehaviour
    {
        [SerializeField] private RawImage bgImage;
        [SerializeField] private float scrollSpeed;
        
        private void Update()
        {
            if (gameObject.activeInHierarchy)
            {
                var newXValue = bgImage.uvRect.x + Time.deltaTime * scrollSpeed;
                bgImage.uvRect = new Rect(new Vector2(newXValue, 0), Vector2.one);
            }
        }
    }
}
