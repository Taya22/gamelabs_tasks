using UnityEngine;

namespace Task_3.Scripts.Modules
{
    public abstract class ShipModule : ScriptableObject
    {
        [SerializeField] private ModularShip.StatType _statType;
        public ModularShip.StatType GetStatType => _statType;

        [SerializeField] public string moduleName;
        [SerializeField] public Sprite sprite;
        [SerializeField] [TextArea] public string description;

        public abstract float Apply(float currentValue);
        public abstract float Detach(float currentValue);
    }
}
