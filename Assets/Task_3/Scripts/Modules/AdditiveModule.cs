using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Task_3.Scripts.Modules
{
    [CreateAssetMenu(menuName = "Task_3/Modules/Additive Module")]
    public class AdditiveModule : ShipModule
    {
        [SerializeField] int amountToAdd = 50;

        public override float Apply(float currentValue)
        {
            return currentValue + amountToAdd;
        }

        public override float Detach(float currentValue)
        {
            return currentValue - amountToAdd;

        }
    }
}
