using UnityEngine;
using UnityEngine.Serialization;

namespace Task_3.Scripts.Modules
{
    [CreateAssetMenu(menuName = "Task_3/Modules/Percentage Module")]
    public class PercentageModule : ShipModule
    {
        [SerializeField] [Range(0f, 1f)] float percentsToAdd = 0.2f;

        public override float Apply(float currentValue)
        {
            return currentValue - 1 * percentsToAdd;
        }

        public override float Detach(float currentValue)
        {
            return currentValue + 1 * percentsToAdd;

        }
    }
}
