using System;
using System.Collections.Generic;
using System.Linq;
using Task_3.Scripts.Modules;
using Task_3.Scripts.UI;
using Task_3.Scripts.Weapons;
using UnityEngine;

namespace Task_3.Scripts
{
    public class ShopManager : MonoBehaviour
    {
        [SerializeField] private DraggableShopItem draggableShopItemPrefab;
        [SerializeField] private ShipModule[] modules;
        [SerializeField] private ShipWeapon[] weapons;
        [SerializeField] private Transform[] moduleShopSlots;
        [SerializeField] private Transform[] weaponShopSlots;

        private List<DraggableShopItem> instantiatedDraggableModules = new List<DraggableShopItem>();
        private List<DraggableShopItem> instantiatedDraggableWeapons = new List<DraggableShopItem>();

        private void Start()
        {
            SetupShopItems();
        }

        private void SetupShopItems()
        {
            for (var i = 0; i < moduleShopSlots.Length; i++)
            {
                if (modules.Length <= i) break;
                InstantiateNewModule(i);
            }

            for (var i = 0; i < weaponShopSlots.Length; i++)
            {
                if (weapons.Length <= i) break;
                InstantiateNewWeapon(i);
            }
        }

        //we can use ObjectPooling here, but for our case it will be too much
        public void ShowNewItem(ShipModule module)
        {
            var index = Array.IndexOf(modules, module);
            if (moduleShopSlots[index].childCount <= 0)
                InstantiateNewModule(index);
            else 
                moduleShopSlots[index].GetChild(0).gameObject.SetActive(true);
            
        } 
        
        public void ShowNewItem(ShipWeapon weapon)
        {
            var index = Array.IndexOf(weapons, weapon);
            if(weaponShopSlots[index].childCount <= 0)
                InstantiateNewWeapon(index);
            else 
                weaponShopSlots[index].GetChild(0).gameObject.SetActive(true);
        }

        private void InstantiateNewModule(int index)
        {
            if (moduleShopSlots.Length <= index)
            {
                Debug.LogWarning("ERROR: Module index > shop slots");
                return;
            }
            
            var newDraggableItem = Instantiate(draggableShopItemPrefab, Vector3.zero, Quaternion.identity);
            newDraggableItem.transform.SetParent(moduleShopSlots[index], false);
            newDraggableItem.Initialize(modules[index]);
            instantiatedDraggableModules.Add(newDraggableItem);
        }
        
        private void InstantiateNewWeapon(int index)
        {
            if (weaponShopSlots.Length <= index)
            {
                Debug.LogWarning("ERROR: Module index > shop slots");
                return;
            }
            
            var newDraggableItem = Instantiate(draggableShopItemPrefab, Vector3.zero, Quaternion.identity);
            newDraggableItem.transform.SetParent(weaponShopSlots[index], false);
            newDraggableItem.Initialize(weapons[index]);
            instantiatedDraggableWeapons.Add(newDraggableItem);
        }
    }
}
