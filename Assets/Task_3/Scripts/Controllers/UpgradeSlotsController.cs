using System;
using System.Collections.Generic;
using Task_3.Scripts.UI;
using UnityEngine;

namespace Task_3.Scripts
{
    public class UpgradeSlotsController : MonoBehaviour
    {
        [SerializeField] private HangarController hangarController;
        [SerializeField] private ShopManager shopManager;
        [SerializeField] private UpgradeSlotsView upgradeSlotsViewPrefab;
        [SerializeField] private List<UpgradeSlotsView> _upgradeSlotsViews;

        private void Awake()
        {
            foreach (var slots in _upgradeSlotsViews)
            {
                slots.OnSpawned(hangarController, shopManager);
            }
        }

        private void OnEnable()
        {
            HangarController.OnShipSelected += OnShipSelected;
        }

        private void OnDisable()
        {
            HangarController.OnShipSelected -= OnShipSelected;
        }

        private void OnShipSelected(ModularShip curShip, int shipIndex)
        {
            foreach (var upgradeSlotsView in _upgradeSlotsViews) {
                upgradeSlotsView.gameObject.SetActive(false);
            }
            
            if (_upgradeSlotsViews[shipIndex] == null) {
                var newSlots = Instantiate(upgradeSlotsViewPrefab, Vector3.zero, Quaternion.identity);
                newSlots.transform.SetParent(transform, false);
                newSlots.OnSpawned(hangarController, shopManager);
                newSlots.OnShipSelected(curShip);
            }
            else
            {
                var slotsView = _upgradeSlotsViews[shipIndex];
                slotsView.OnShipSelected(curShip);
                slotsView.gameObject.SetActive(true);
            }
        }

    }
}
