using System;
using System.Collections.Generic;
using DG.Tweening;
using Task_3.Scripts.Modules;
using Task_3.Scripts.Weapons;
using UnityEngine;
using UnityEngine.Events;

namespace Task_3.Scripts
{
    public class HangarController : MonoBehaviour
    {
        public static Action<ModularShip, int> OnShipSelected;

        [SerializeField] List<ModularShip> _modularShips;
        private ModularShip prevShip;
        private ModularShip curShip;
        private int curShipIndex = 0;

        private void Start()
        {
            SelectShip(0, false);
        }

        public void OnAttachModule(ShipModule module)
        {
            curShip.AttachModule(module);
        }

        public void OnDetachModule(ShipModule module)
        {
            curShip.DetachModule(module);
        }

        public void OnAttachWeapon(ShipWeapon weapon)
        {
            curShip.AttachWeapon(weapon);
        }

        public void OnDetachWeapon(ShipWeapon weapon)
        {
            curShip.DetachWeapon(weapon);
        }

        public void SwitchShip(bool isRight)
        {
            prevShip = _modularShips[curShipIndex];

            if (isRight)
            {
                if (curShipIndex < _modularShips.Count - 1) curShipIndex++;
                else curShipIndex = 0;
            }
            else
            {
                if (curShipIndex > 0) curShipIndex--;
                else curShipIndex = _modularShips.Count - 1;
            }

            ShowSelectedShip(isRight);
            curShip = _modularShips[curShipIndex];
            OnShipSelected.Invoke(curShip, curShipIndex);
        }

        private void SelectShip(int index, bool animateAppearance = true)
        {
            prevShip = _modularShips[curShipIndex];

            curShipIndex = index;
            curShip = _modularShips[curShipIndex];

            OnShipSelected?.Invoke(curShip, curShipIndex);
            StartRotationLoop(curShip.transform);
            if (animateAppearance) ShowSelectedShip();
            else curShip.transform.localPosition = Vector3.zero;
        }
        
        //needs to be reworked
        public void SpawnShipsFromHangar()
        {
            SpawnShip(_modularShips[0], true);
            SpawnShip(_modularShips[1], false);
        }

        public void ReturnShipsToHangar()
        {
            ReturnShip(_modularShips[0]);
            ReturnShip(_modularShips[1]);
        }

        private void SpawnShip(ModularShip ship, bool leftSide)
        {
            ship.transform.SetParent(null);
            ship.transform.DOKill();
            ship.transform.rotation = leftSide ? Quaternion.identity : Quaternion.Euler(0, 180, 0);
            ship.transform.position = leftSide ? Vector3.left * 30 : Vector3.right * 30;
            ship.OnSpawnedFromHangar(ship == curShip);
        }

        private void ReturnShip(ModularShip ship)
        {
            ship.transform.SetParent(transform.GetChild(0), false);
            ship.transform.position = Vector3.right * -20f;
            ship.transform.rotation = Quaternion.Euler(-15, 35, 0);
            SelectShip(0, false);
            ship.OnReturnToHangar();
        }

        //we can disable deselected ships in .OnComplete, but for now we don't need it
        private void ShowSelectedShip(bool fromRight = true)
        {
            curShip = _modularShips[curShipIndex];

            prevShip.transform.DOKill();
            curShip.transform.DOKill();

            curShip.transform.localPosition = fromRight ? Vector3.right * 20 : Vector3.right * -20;
            prevShip.transform.DOLocalMoveX(fromRight ? -20 : 20, .7f);
            curShip.transform.DOLocalMoveX(0f, .7f);
            StartRotationLoop(curShip.transform);
        }

        private void StartRotationLoop(Transform ship)
        {
            ship.transform.DOLocalRotate(new Vector3(0, 360, 0), 10, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetEase(Ease.Linear)
                .SetRelative(true);
        }
    }
}
