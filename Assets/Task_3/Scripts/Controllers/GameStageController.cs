using System;
using UnityEngine;

namespace Task_3.Scripts.Controllers
{
    //not really a good approach, but I don't have much time left
    public class GameStageController : MonoBehaviour
    {
        [SerializeField] private HangarController hangarController;
        [Header("Canvases")]
        [SerializeField] private Canvas menuCanvas;
        [SerializeField] private Canvas gameCanvas;
        
        [Header("cameras")]
        [SerializeField] private Camera menuCamera;
        [SerializeField] private Camera gameCamera;

        public void StartMenuStage()
        {
            menuCanvas.gameObject.SetActive(true);
            gameCanvas.gameObject.SetActive(false);
            
            menuCamera.gameObject.SetActive(true);
            gameCamera.gameObject.SetActive(false);
            hangarController.ReturnShipsToHangar();
        }
        
        public void StartGameStage()
        {
            menuCanvas.gameObject.SetActive(false);
            gameCanvas.gameObject.SetActive(true);
            
            menuCamera.gameObject.SetActive(false);
            gameCamera.gameObject.SetActive(true);
            hangarController.SpawnShipsFromHangar();
        }
    }
}
