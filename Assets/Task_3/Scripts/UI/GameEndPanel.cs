using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameEndPanel : MonoBehaviour
{
    [SerializeField] private TMP_Text gameEndText;
    [SerializeField] private GameObject panel;

    private void OnEnable()
    {
        EventManager.OnShipDestroyed.AddListener(OnGameEnd);
    }

    private void OnDisable()
    {
        panel.SetActive(false);
    }

    private void OnGameEnd(bool playerLost)
    {
        if (playerLost)
        {
            gameEndText.text = "You Lose!";
            gameEndText.color = Color.red;
        }
        else
        {
            gameEndText.text = "You Won!";
            gameEndText.color = Color.yellow;
        }
        
        panel.SetActive(true);
    }
}
