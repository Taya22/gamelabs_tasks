using UnityEngine;
using UnityEngine.EventSystems;

namespace Task_3.Scripts.UI
{
    public class UpgradeSlots : MonoBehaviour, IDropHandler
    {
        private enum SlotType { Module, Weapon }

        [SerializeField] private SlotType slotType;
        
        //consider using events
        private HangarController _hangarController;
        private ShopManager _shopManager;

        public void Initialize(HangarController hangarController, ShopManager shopManager)
        {
            _hangarController = hangarController;
            _shopManager = shopManager;
        }
    
        public void OnDrop(PointerEventData eventData)
        {
            if(IsOccupied()) return;
            
            var dropped = eventData.pointerDrag;
            var draggableItem = dropped.GetComponent<DraggableShopItem>();
            
            if(!draggableItem) return;

            switch (slotType)
            {
                case SlotType.Module:
                    if(draggableItem.IsAttached || draggableItem.Module == null)
                        return;
                    _hangarController.OnAttachModule(draggableItem.Module);
                    _shopManager.ShowNewItem(draggableItem.Module);
                    break;
                case SlotType.Weapon:
                    if(draggableItem.IsAttached || draggableItem.Weapon == null)
                        return;
                    _hangarController.OnAttachWeapon(draggableItem.Weapon);
                    _shopManager.ShowNewItem(draggableItem.Weapon);
                    break;
            }

            draggableItem.IsAttached = true;
            draggableItem.parentAfterDrag = transform;
        }

        private bool IsOccupied()
        {
            var occupied = false;
            
            for (var i = 0; i < transform.childCount; i++)
            {
                if (!transform.GetChild(i).gameObject.activeInHierarchy) continue;
                occupied = true;
                break;
            }

            return occupied;
        }
    }
}
