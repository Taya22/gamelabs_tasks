using UnityEngine;
using UnityEngine.EventSystems;

namespace Task_3.Scripts.UI
{
    public class DetachDropCatcher : MonoBehaviour, IDropHandler
    {
        [SerializeField] private HangarController _hangarController;
        public void OnDrop(PointerEventData eventData)
        {
            var dropped = eventData.pointerDrag;
            var draggableItem = dropped?.GetComponent<DraggableShopItem>();
            
            if(!draggableItem || !draggableItem.IsAttached) return;

            if (draggableItem.Module != null)
            {
                _hangarController.OnDetachModule(draggableItem.Module);
                ResetItem(draggableItem);
                return;
            }

            if (draggableItem.Weapon != null)
            {
                _hangarController.OnDetachWeapon(draggableItem.Weapon);
                ResetItem(draggableItem);
                return;
            }
        }

        private void ResetItem(DraggableShopItem draggableItem)
        {
            draggableItem.IsAttached = false;
            draggableItem.parentAfterDrag = draggableItem.initialParent;
            draggableItem.gameObject.SetActive(false);
            draggableItem.ResetItem();
        }
    }
}
