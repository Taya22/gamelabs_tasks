using System;
using Task_3.Scripts.Modules;
using TMPro;
using UnityEngine;

namespace Task_3.Scripts.UI
{
    public class ShipStats : MonoBehaviour
    {
        [SerializeField] private TMP_Text healthText;
        [SerializeField] private TMP_Text shieldText;
        private ModularShip curShip;

        #region Events Handling

        private void OnEnable()
        {
            HangarController.OnShipSelected += OnShipSelected;
        }

        private void OnDisable()
        {
            HangarController.OnShipSelected -= OnShipSelected;
        }

        #endregion

        private void OnShipSelected(ModularShip ship, int shipIndex)
        {
            if (curShip) {
                curShip.OnStatValueChange -= OnStatValueChanged;
            }

            curShip = ship;
            
            healthText.text = curShip.GetShipHealth().ToString();
            shieldText.text = curShip.GetShipShield().ToString();
            
            curShip.OnStatValueChange += OnStatValueChanged;
        }

        private void OnStatValueChanged(ModularShip.StatType statType, float newValue)
        {
            switch (statType)
            {
                case ModularShip.StatType.Health:
                    healthText.text = $"{newValue} HP";
                    break;
                case ModularShip.StatType.Shield:
                    shieldText.text = $"{newValue} SH";
                    break;
                default:
                    return;
            }
        }
    }
}
