using Task_3.Scripts.Modules;
using Task_3.Scripts.Weapons;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Task_3.Scripts.UI
{
    public class DraggableShopItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private Image itemImage;
        [SerializeField] private TMP_Text itemText;
        
        [HideInInspector] public Transform parentAfterDrag;
        [HideInInspector] public Transform initialParent;
        
        [HideInInspector]public ShipModule Module;
        [HideInInspector]public ShipWeapon Weapon;

        public bool IsAttached;
        
        public void Initialize (ShipModule module)
        {
            Module = module;
            itemImage.sprite = module.sprite;
            itemText.text = module.moduleName;
            initialParent = transform.parent;
        }
         
        public void Initialize (ShipWeapon weapon)
        {
            Weapon = weapon;
            itemImage.sprite = weapon.sprite;
            itemText.text = weapon.weaponName;
            initialParent = transform.parent;
        }
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            itemImage.raycastTarget = false;
            parentAfterDrag = transform.parent;
            transform.SetParent(transform.root);
            transform.SetAsLastSibling();
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            var screenPoint = Input.mousePosition;
            screenPoint.z = 100;
            transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ResetItem();
        }

        public void ResetItem()
        {
            itemImage.raycastTarget = true;
            transform.SetParent(parentAfterDrag);
            transform.localPosition = Vector3.zero;
        }
    }
}
