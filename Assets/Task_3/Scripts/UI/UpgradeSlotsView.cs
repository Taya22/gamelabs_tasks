using System;
using System.Collections.Generic;
using Task_3.Scripts.Modules;
using Task_3.Scripts.UI;
using Task_3.Scripts.Weapons;
using UnityEngine;

namespace Task_3.Scripts
{
    public class UpgradeSlotsView : MonoBehaviour
    {
        [SerializeField] private UpgradeSlots[] moduleSlots;
        [SerializeField] private UpgradeSlots[] weaponSlots;

        public void OnSpawned(HangarController hangarController, ShopManager shopManager)
        {
            InitializeSlots(moduleSlots, hangarController, shopManager);
            InitializeSlots(weaponSlots, hangarController, shopManager);
        }
        
        private void InitializeSlots(UpgradeSlots[] slots, HangarController hangarController, ShopManager shopManager)
        {
            foreach (var slot in slots) {
                slot.Initialize(hangarController, shopManager);
            } 
        }
        
        public void OnShipSelected(ModularShip curShip)
        {
            DeactivateAllSlots();
            ActivateSlots(curShip);
        }
        
        private void DeactivateAllSlots()
        {
            foreach (var slot in moduleSlots)
            {
                slot.gameObject.SetActive(false);
            }
            foreach (var slot in weaponSlots)
            {
                slot.gameObject.SetActive(false);
            }
        }

        private void ActivateSlots(ModularShip curShip)
        {
            for (var i = 0; i < curShip.ModulesSlots; i++)
            {
                moduleSlots[i].gameObject.SetActive(true);
            }
            
            for (var i = 0; i < curShip.WeaponSlots; i++)
            {
                weaponSlots[i].gameObject.SetActive(true);
            }
        }
        
    }
}
