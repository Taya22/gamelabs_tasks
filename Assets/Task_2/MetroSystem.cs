using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Task_2
{
    public class MetroSystem : MonoBehaviour
    {
        private struct Path
        {
            public List<Station> Stations;
            public int Transfers;

            public Path(List<Station> stations, int transfers)
            {
                Stations = stations;
                Transfers = transfers;
            }
        }
        
        [SerializeField] private Station startStation;
        [SerializeField] private Station endStation;
        
        public void FindPath()
        {
            var path = FindShortestPath(startStation, endStation);

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var station in path.Stations)
            {
                stringBuilder.Append($" {station.name}");
            }

            Debug.Log(stringBuilder);
            Debug.Log(path.Transfers);
        }

        private Path FindShortestPath(Station startStation, Station endStation)
        {
            Queue<Station> queue = new Queue<Station>();
            HashSet<Station> exploredStations = new HashSet<Station>();
            Path path = new Path(new List<Station>(), 0);

            if (startStation == endStation) {
                Debug.Log("You're on the destination station");
                return path;
            }
            
            queue.Enqueue(startStation);

            while (queue.Count > 0)
            {
                var curStation = queue.Dequeue();
                
                foreach (var connectedStation in curStation.ConnectedStations)
                {
                    if (exploredStations.Contains(connectedStation)) continue;
                    
                    exploredStations.Add(connectedStation);
                    connectedStation.prevStation = curStation;

                    if (connectedStation == endStation)
                    {
                        path = CreatePath(connectedStation, startStation);
                        break;
                    }

                    queue.Enqueue(connectedStation);
                }
            }

            return path;
        }

        private Path CreatePath(Station destination, Station origin)
        {
            List<Station> stations = new List<Station>();
            Station current = destination;
            var transfersCount = 0;
            
            while (current != origin)
            {
                stations.Add(current);
                if (current.prevStation != null)
                {
                    var curLine = GetCurrentLine(current);
                    var prevLine = GetCurrentLine(current.prevStation);
                    if (curLine != prevLine) transfersCount++;
                    current = current.prevStation;
                }
                else 
                    break;
            }
            
            stations.Add(origin);
            stations.Reverse();

            Path path = new Path(stations, transfersCount);
            return path;
        }

        private MetroLine GetCurrentLine(Station current)
        {
            foreach (var currentLine in current.ConnectedLines)
            {
                foreach (var prevLine in current.prevStation.ConnectedLines)
                {
                    if (currentLine != prevLine) continue;
                    return currentLine;
                }
            }

            return null;
        }
    }
}
