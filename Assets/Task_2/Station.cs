using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Task_2
{    
    [CreateAssetMenu(menuName = "Task_2/Station")]
    public class Station : ScriptableObject
    {
        public string Name;
        public List<MetroLine> ConnectedLines;
        public List<Station> ConnectedStations;
        [HideInInspector]public Station prevStation;
    }
}
