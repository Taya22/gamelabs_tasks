using System.Collections.Generic;
using UnityEngine;

namespace Task_2
{
    [CreateAssetMenu(menuName = "Task_2/Metro Line")]
    public class MetroLine : ScriptableObject
    {
        public string Name;
        public List<Station> Stations;
    }
}
