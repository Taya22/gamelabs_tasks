using System;
using UnityEngine;

public class FlagGenerator : MonoBehaviour
{
    [SerializeField] private int width = 10;
    [SerializeField] private int height = 10;
    [SerializeField] private Material mat;
    [SerializeField] private Texture texture;

    private void OnValidate()
    {
        mat.mainTexture = texture;
    }

    private void Start()
    {
        CreateFlag();
    }

    private void CreateFlag()
    {
        GameObject flag = new GameObject("Flag");
        flag.transform.SetParent(transform);

        GenerateFlag(out Mesh mesh);

        flag.AddComponent<MeshFilter>().mesh = mesh;
        var meshRenderer = flag.AddComponent<MeshRenderer>();
        meshRenderer.material = mat;
    }

    private void GenerateFlag(out Mesh outMesh)
    {
        var vertices = new Vector3[(width + 1) * (height + 1)];
        var triangles = new int[width * height * 6];
        Vector2[] uvs = new Vector2[vertices.Length];

        for (int i = 0, y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++, i++)
            {
                vertices[i] = new Vector3(x, 0, y);
                uvs[i] = new Vector2((float)x / width, (float)y / height);
            }
        }

        for (int ti = 0, vi = 0, y = 0; y < height; y++, vi++)
        {
            for (int x = 0; x < width; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 1] = vi + width + 1;
                triangles[ti + 2] = vi + 1;


                triangles[ti + 3] = vi + 1;
                triangles[ti + 4] =  vi + width + 1;
                triangles[ti + 5] = vi + width + 2;
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();

        outMesh = mesh;
    }
}